package com.demo.springboot.data;

import com.demo.springboot.validators.PeselValidator;

public class PeselDto {
    private String pesel;

    public PeselDto(String pesel) {
        this.pesel = pesel;
    }
    public boolean checkPesel() {
        PeselValidator validator = new PeselValidator();
        return validator.peselValidator(pesel);
    }
}
