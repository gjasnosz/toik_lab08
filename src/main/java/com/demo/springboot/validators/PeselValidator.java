package com.demo.springboot.validators;

public class PeselValidator {

    private final int LENGTH = 11;
    int[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

    public boolean peselValidator(String pesel) {
        if (pesel.length() != LENGTH) {
            return false;
        } else {
            int sum = 0;
            int controlSum;
            for (int i = 0; i < LENGTH - 1; i++) {
                sum += Integer.valueOf(String.valueOf(pesel.charAt(i))) * weights[i];
            }
            if (sum % 10 != 0) {
                controlSum = 10 - sum % 10;
            } else {
                controlSum = 0;
            }
            return controlSum == Integer.valueOf(pesel.substring(LENGTH - 1));

        }
    }
}
